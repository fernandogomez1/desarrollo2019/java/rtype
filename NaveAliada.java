//Punto nº7 de la práctica
//Punto nº8 de la práctica
//punto nº9 de la practica.
import java.util.ArrayList;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import java.awt.Image;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import javax.swing.JPanel;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyAdapter;
import java.awt.Rectangle;

public class NaveAliada extends Nave
{
private int dx;//desplazamiento x
private int dy;//desplazamiento y
private ArrayList<Misil> MisilesActivos;//array de misiles
private static final int TAMANO=50;

public NaveAliada(int ejeX, int ejeY, String icono)
    {
    EjeX=ejeX;
    EjeY=ejeY;
    Visible=true;
    ImageIcon ii = new ImageIcon(this.getClass().getResource(icono));//se crea un objeto de tipo icono
    INave = ii.getImage();//obtiene al imagen
    MisilesActivos=new ArrayList<Misil>();//se crea el array
    
    width=INave.getWidth(null); //getWidth es un metodo de la clase image, obtiene el ancho de la imagen en pixeles.
    height = INave.getHeight(null);//obtiene el alto de la imagen en pixeles.
    mover();//llama al metodo mover
    }
    
    public void mover()//cuando se detecte una pulsación  de tecla se llama a este metodo
            {
                /**
                 * Los siguientes if, son para limitar el movimiento de la nave aliada y que no se salga de los limites 
                 * de la pantalla.
                 */
            if (x< 0)//limite izquierdo
            {
            x=0;
            }
            if (x>EjeX-width)//limite derecho
            {
            x=EjeX-width;
            }
            if(y<0)//limite superior
            {
            y=0;
            }
            /**
             *El -20 se pone porque a pesar de calcular el alto, 
             * no llega a salir la nave entera cuando llega abajo del todo
             */
            if(y>EjeY-height-20)//limite inferior. 
            {

            y=EjeY-height-20;
            }
            x += dx;//desplazamiento x
            
            y += dy;//desplazamiento y
        }
      
        
        
        
        
        public void keyPressed(KeyEvent e)//trabaja junto con la clase Tadapter, esta clase esta dentro de la clase Controlador.

    {
        int key = e.getKeyCode();
        if(key==KeyEvent.VK_SPACE)//si se pulsa espacio, se llama al metodo disparar de esta clase.
        {
        Disparar();
        }
        
        if(key == KeyEvent.VK_O)//si se ha pulsado la tecla O, la nave se mueve a la izquierda
            {
            dx = -1;
            }
        if(key == KeyEvent.VK_P)//si se ha pulsado la tecla p, la nave se mueve a la derecha
            {
            dx = 1;
            }
        if(key == KeyEvent.VK_Q)//si se ha pulsado la tecla q, la nave se mueve a arriba
            {
            dy = -1;
            }
        if(key == KeyEvent.VK_A)//si se ha pulsado la tecla a, la nave se mueve a abajo
            {
            dy = 1;
            }
    }
    
    
    public void keyReleased(KeyEvent e)//si se deja de pulsar la tecla....
    {
    int key = e.getKeyCode();
    if(key == KeyEvent.VK_O)//......"o", la nave se deja de mover a la izquierda
        {
        dx=0;
        }
        if(key == KeyEvent.VK_P)//......"p", la nave se deja de mover a la derecha
        {
        dx=0;
        }
        if(key == KeyEvent.VK_Q)//......"q", la nave se deja de mover a arriba
        {
        dy=0;
        }
        if(key == KeyEvent.VK_A)//......"a", la nave se deja de mover a abajo.
        {
        dy=0;
        }
    }
    /**
     * cada vez que se llama a este metodo, se añade un nuevo objeto al array de tipo misil.
     */
    public void Disparar()
    {
    MisilesActivos.add(new Misil(x+TAMANO, y + TAMANO/2, EjeX));
    
    }
    /**
     * retorna el array de misiles.
     */
    public ArrayList<Misil>getMissiles()
        {
         return MisilesActivos;
        }  
        
}