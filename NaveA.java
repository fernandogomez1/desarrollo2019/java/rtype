//Punto nº 5 de la práctica
//punto nº11a de la práctica.
//punto nº13 de la practica
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import java.awt.Rectangle;

public class NaveA extends Nave
{
    private int auxX;//variable auxiliar, para recordar la posicion "x" en la que debe reaparecer la nave.
    public NaveA(int ejeX, int ejeY, String RIcono)
    {
    EjeX=ejeX;
    EjeY=ejeY;
    
    Visible=true;//poner nave visible
    ImageIcon ii = new ImageIcon(this.getClass().getResource(RIcono));
    INave = ii.getImage();
    width=INave.getWidth(null); //guarda en la variable width el ancho del icono.
    height = INave.getHeight(null);//guarda en la variable height el alto del icono.
    y=getAleatorio();//esto inicializa el eje "y" de la nave de manera aleatorio.
    if(y<20)
    {
    y=20;
    }
    if (y>548)
    {
    y=548;
    }
    x=getAleatorio()+600;//esto inicializa la nave en una coordenada "x" aleatoria fuera del tablero.
    auxX=x;//como el valor x va a ir variando, aux almacena el valor original de x para que reaparezca en la misma posicion.
    mover();//se llama al metodo mover para que nada mas crearse el objeto, se empiece a desplazar la NaveA
    }
    
    public void mover()
    {
    
        if (x<0) //si llega al limite izquierdo del tablero, se reubica la nave al punto de partida.
        {
        x=auxX;
        }
        else//si no se ha alcanzado el limite izquiero del tablero, se desplaza la nave un pixel.
        {
        x=x-1;
        }
    }
    
}