import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
public class Mensajes
{
   public Image imagen;
    public Mensajes(String ruta)
    {
    ImageIcon ii = new ImageIcon(this.getClass().getResource(ruta));
    imagen = ii.getImage();   
    }

  public Image getImage()
        {
        return imagen;
        }
        
  
}
