import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import java.awt.Rectangle;
import java.util.Random;
public class Nave
{
   public int numero;//esta variable recoge el numero aleatorio de random.
   public Random GeneraNumero;//este objto genera con el metodo getAleatorio, un numero aleatorio.
   public String RutaIcono;//en esta variable se almacena la ruta a la imagen que se va a usar
   public int x, y, EjeX, EjeY; //en la variable "x" e "y" se guardan las coordenadas "x" e "y" de la nave enemiga y en EjeX, EjeY, se guardan los limites del tablero.
   public Image INave;//este objeto almacena el Icono de la Nave.
   public int width;//Almacena el ancho de pixeles de la nave.
   public int height;//almacena el alto de piexles de la nave.
   public boolean Visible;//Con el metodo correspondiente, dice si la nave enemiga es o no visible.
    public Nave()
    {
    GeneraNumero = new Random();
    }

    public int getAleatorio() //Metodo que genera un numero aleatorio entre 1 y el limite de pixeles del eje y del JFrame
    {
        numero=GeneraNumero.nextInt(EjeY)+1;
        return numero;
    }
    
     public int getAleatorioM() //Metodo que genera un numero aleatorio entre 1 y 600 para determinar el movimiento que va a realizar NaveB
    {
        numero=GeneraNumero.nextInt(600)+1;
        return numero;
    }
    
    public int getX() //devuelve la coordenada "x" de la nave enemiga.
        {
        return x;
        }
        
        
    public int getY()//devuelve la coordenada "y" del a nave enemiga.
        {
        return y;
        }
    
    public Image getImage()//devuelve la imagen de la nave enemiga
        {
        return INave;
        }
        
        
        
        /**
          *Para facilitar la detección de colisiones, este metodo
          *conviernte en rectangulo la imagene de la nave.
           */
        
    public Rectangle getBounds()
        {
        return new Rectangle(x, y, width, height);
        }

        /**
           *Este metodo permite modificar la variable Visible a verdader o falso 
           *segun necesidades.
           */
        
    void setVisible (boolean visibleSN)
    {
        
     Visible=visibleSN;
    }
    
    /** 
       *Este metodo devuelve el estado actual de la nave, puede ser visible o invisible.
       */
    boolean EsVisible()
    {
    return Visible;
    }
}