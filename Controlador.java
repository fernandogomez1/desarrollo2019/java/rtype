//punto nº 3 de la práctica.
//punto nº4 de la práctica
//punto nº6 de la práctica
//punto nº12 de la practica. (timer)
//punto nº 14 de la practica
//añadir nº15 de la práctica.


import java.util.ArrayList;
import java.awt.Image;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import javax.swing.JPanel;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.Timer;
import java.awt.event.KeyListener;
import java.awt.event.ActionListener;
import java.awt.Rectangle;
import java.util.TimerTask; //para poder parar el timer unos segundos


    public class Controlador extends JPanel implements ActionListener

    { 
     private Timer timer;//objeto de tipo timer, para llevar a cabo una serie de tareas cada determiando tiempo.
     private NaveAliada navealiada;//objeto tipo NaveAliada
     private int aux=0;//variable auxiliar(mas adeante veremos para que se usa)
     private boolean sw;//switch
     private int i;
     private ArrayList<NaveA> ArrayNaveA;//Array para almacenar las naves de tipo A
     private ArrayList<NaveB> ArrayNaveB;//array para almacenar las naves de tipo b
 
     private int NavesARestantes;
     private int NavesBRestantes;
     private String RutaNaveAliada="naveAliadaP.jpg"; //aquí se escribe la ruta al icono que llevara la nave aliada.
     private String RutaNaveA="NaveA.png";//aquí se escribe la ruta al icono que llevara la nave a.
     private String RutaNaveB="NaveB.png";//aquí se escribe la ruta al icono que llevara la nave b.
     private String RutaLose="gameover.jpeg";
     private String RutaWin="win.jpg";
     private int nnavesa, nnavesb, width, height;
     private Image Win, Lose; //Aqui se guarda la imagen de fin de juego
     private Mensajes mensaje;
     private Mensajes mensajewin;

    
     
     /**
      * Para no tener que ir clase por clase a la hora de cambiar por ejemplo los iconos si se han hecho iconos nuevos o la resolucion del JFrame,
      * se le pasa como parametro los valores necesarios a cada constructor, y así con tan solo modiciar los valores en la clase controlador, 
      * no hace falta editar el resto de clases.
      * 
      * En este caso NNaves es el numero total de naves, Limite es la recolucion del JFrame y velocidad es el tiempo del timer que tarda en repetir cada serie de tareas.
      * que varía en función del nivel. Desde JButon se dejan predefinitos un numero de naves.
      */
    public Controlador(int NNavesA, int NNavesB,int LimiteX, int LimiteY, int velocidad) 
    
    {
                

        
                NavesARestantes=NNavesA;//se pasa el maximo numero de naves a una variable para que pueda ser usada en la calse entera.
                NavesBRestantes=NNavesB;
                nnavesa=NNavesA;//nnavesa es para que se mantenga el numero de naves en los bucles for.
                nnavesb=NNavesB;//nnavesb  es para que se mantenga el numero de naves en los bucles for.
                navealiada=new NaveAliada(LimiteX, LimiteY, RutaNaveAliada);//se crea un objeto de tipo NaveAliada y se le pasan los valores necesarios al constructor.
                mensaje=new Mensajes(RutaLose);//se crea un objeto de tipo mensaje, al cual se le pasa la ruta de la imagen png o jpg amostrar.
                mensajewin=new Mensajes(RutaWin);//se crea un objet de tipo mensaje, al cual se le pasa la ruta de la imagen png o jpg amostrar.
                ArrayNaveA=new ArrayList<NaveA>();//se crea una array para almacenar un numero determinado de naves tipo a.
                ArrayNaveB=new ArrayList<NaveB>();//se crea una array para almacenar un numero determinado de naves tipo b.
                
                    for(i=0;i<nnavesa;i++)//inicializa una aray de naves tipo a
                    {    
                    ArrayNaveA.add(new NaveA(LimiteX, LimiteY, RutaNaveA));
                    }

                    
                for(i=0;i<nnavesb;i++)//inicializa una array de naves tipo b
                {
                    
                ArrayNaveB.add(new NaveB(LimiteX, LimiteY, RutaNaveB));//añade naves tipo b al array
                }
                setBackground(Color.BLACK);//color de fondo del juego.
                setDoubleBuffered(true);

                this.addKeyListener(new TAdapter());//se usa esto para que funcione el teclado. En el pdf se detalla mas su funcionamiento.
                timer = new Timer(velocidad, this);//se crea un nuevo objeto de tipo Timer para que ejecute una serie de tareas cada cierto tiempo.
                timer.start();//se inicia la serie de tareas definidas.

        
    }

    public void paint(Graphics g)//este metodo permite dibujar en el tablero cada uno de los elementos del juego sobreescriiendolos
    {
                super.paint(g);//llama al metodo paint de la superclase para hacer la inicializacion del jpanel.
                Graphics2D g2d=(Graphics2D)g;//contexto en 2 dimensiones
                for(i=0; i<nnavesa; i++)//si la naveA es visible la dibuja, si no no
                    {
                    NaveA navea=(NaveA) ArrayNaveA.get(i); //esto permite llevar un control de la nave actual en una posicion determinada del array.
                    if(navea.EsVisible()==true)//si la naves es visible
                    {
                    g2d.drawImage(navea.getImage(), navea.getX(), navea.getY(), this);//dibuja la nave a en la coordenada que retorne los metodos getX y getY
                    }
                    }
                    
                    /**
                     * Para el resto de for, el funcionamiento es el mismo que el for de navesa
                     */
                for(i=0; i<nnavesb; i++)//si naveB es visible la dibuja, sino no
                    {
                    NaveB naveb=(NaveB) ArrayNaveB.get(i); 
                    if(naveb.EsVisible()==true)
                    {
                    g2d.drawImage(naveb.getImage(), naveb.getX(), naveb.getY(), this);
                    }
                    }
                
                
                
                
                ArrayList<Misil> ms = navealiada.getMissiles();
                
                for (int j=0 ; j< ms.size() ; j++)//si el misil es visible lo dibuja.
                    {
                    Misil m=(Misil) ms.get(j);
                    if(m.EsVisible()==true)//esto hace que al impactar se elimine el misil y no siga eliminando las naves que tiene detras (las que estan en el mismo eje y)
                    {
                    g2d.drawImage(m.getImage(), m.getX(), m.getY(), this);
                    }
                    }
                    
              
               if (navealiada.EsVisible()==true)//si la nave aliada es visible la dibuja
               {
                   g2d.drawImage(navealiada.getImage(), navealiada.getX(), navealiada.getY(), this);


                }
                else
                {
                    g2d.drawImage(mensaje.getImage(),0, 0, this);//si la nave aliada no es visible, quiere decir que ha termiando la partida, por lo tanto, se muestra el mensaje de fin de partida.
                    
                    //setVisible(false);
                                 new RType(); //se crea un nuevo objeto de tipo RType para que muestre el menu principal de selección de nivel
                                 
                    timer.stop();//como se ha terminado la partida, ya no necesitamos repetir una seria de tareas, por lo tanto, se para la ejecución del timer.
                        
                }
                 if (NavesARestantes==0 && NavesBRestantes==0)//si no quedan naves enemigas, quiere decir que has ganado la partida, por lo tanto, muestra el mensaje correspondiente.
               {

                                   g2d.drawImage(mensajewin.getImage(),0, 0, this);
                                   
                                 //setVisible(false);
                                 new RType();//se crea un nuevo objeto de tipo RType para que muestre el menu principal de selección de nivel
                                 
                                 timer.stop();//como se ha terminado la partida, ya no necesitamos repetir una seria de tareas, por lo tanto, se para la ejecución del timer.
                                   
               }
                Toolkit.getDefaultToolkit().sync();//permite enlazar las clases con la implementacon del sistema
                grabFocus(); 
                g.dispose();//para que borre lo pintado
                
    
    }

    public void actionPerformed(ActionEvent e)//Estas son las tareas que realiza timer. Se detallan en el pdf
    {
                
                for(i=0;i<ArrayNaveA.size();i++)
                {
                    NaveA navea=(NaveA) ArrayNaveA.get(i);
                navea.mover();
               
            
                }
                
                for(i=0;i<ArrayNaveB.size();i++)
                {
                    NaveB naveb=(NaveB) ArrayNaveB.get(i);
                naveb.mover();
              
            
                }
              
                ArrayList<Misil> ms = navealiada.getMissiles();
                for (int i= 0 ; i < ms.size();i++)
                    {
                        Misil m=(Misil) ms.get(i);
                        if(m.EsVisible())
                        m.Mover();
                        else
                        ms.remove(i);
                    }
                

                navealiada.mover();
                CompruebaColisionesNAlNA();
                CompruebaColisionesMNA();
                CompruebaColisionesNBNAl();
                CompruebaColisionesMNB();
                
            
                repaint(); //vuelve a ejecutar el metodo paint
              
    }

//se le pasan los eventos de teclado a la clase correspondiente para así cambiar sus campos.
    class TAdapter extends KeyAdapter {

            public void keyReleased(KeyEvent e) {
    
                 navealiada.keyReleased(e);
    
            }
    
            public void keyPressed(KeyEvent e) {
    
                 navealiada.keyPressed(e);
    
            }
    }
    
    
    //r1 misil
    //r2 NaveA
    //r3 NaveAliada
    //r4 NaveB
   public void CompruebaColisionesNAlNA() //comprueba colisiones de naveA con NaveAliada
        {
                    
                Rectangle r3 = navealiada.getBounds();//Rectangle es una clase predefinira.
                for (int j = 0;j<ArrayNaveA.size(); j++)
                {
                NaveA navea=(NaveA) ArrayNaveA.get(j);
                Rectangle r2= navea.getBounds();

                if (r3.intersects(r2)&&navea.EsVisible()==true) //intersects pertenece a la clase rectangulo, es un metodo y comprueba si un objeto tipo imagen esta encima del otro
                {
                navealiada.setVisible(false);//en caso de que se detecte colision, cambia el estado del objeto.
                navea.setVisible(false);
                
                }
                }
                
                
          }
               
          
          /**
           * la descripcion es la misma para el resto de colisiones que como se ha comentado en el metodo CompruebaColisionesNAlNA()
           */
 public void CompruebaColisionesMNA() //comprueba colisiones de Misil con Nave tipo A
 {
           
                 ArrayList ms= navealiada.getMissiles();
                for(int i=0; i<ms.size();i++)
                    {
                    Misil m =(Misil) ms.get(i);
                    Rectangle r1 = m.getBounds(); //misil es r1
                        
                    for (int j=0; j<ArrayNaveA.size(); j++)
                            {
                            NaveA navea=(NaveA) ArrayNaveA.get(j);
                            Rectangle r2=navea.getBounds();//r2 es navePrueba
                            
                            if(r1.intersects(r2) && m.EsVisible()==true && navea.EsVisible()==true)
                            {
                                NavesARestantes=NavesARestantes-1;

                                
                            m.setVisible(false);//m es misil
                            navea.setVisible(false);
                            
                            }
                    }
                
                }
        }
        
    //r1 misil
    //r2 NaveA
    //r3 NaveAliada
    //r4 NaveB
    
public void CompruebaColisionesNBNAl() //Comprueba colisiones de Nave tipo B contra la nave Aliada
{
   Rectangle r5 = navealiada.getBounds();//Rectangle es una clase predefinida.
 for (int j = 0;j<ArrayNaveB.size(); j++)
                {
                NaveB naveb=(NaveB) ArrayNaveB.get(j);
                Rectangle r4= naveb.getBounds();
                //colisiones naveAliada con navesTipoB
                if (r5.intersects(r4)&&naveb.EsVisible()==true) //intersects pertenece a la clase rectangulo, es un metodo y comprueba si uno esta encima del otro
                {
                navealiada.setVisible(false);
                naveb.setVisible(false);
                
                }
                }

}

 public void CompruebaColisionesMNB() //comprueba colisiones de Misil con Nave tipo B
 {
           
                 ArrayList ms= navealiada.getMissiles();
                for(int i=0; i<ms.size();i++)
                    {
                    Misil m =(Misil) ms.get(i);
                    Rectangle r1 = m.getBounds(); //misil es r1
                        
                    for (int j=0; j<ArrayNaveB.size(); j++)
                            {
                            NaveB naveb=(NaveB) ArrayNaveB.get(j);
                            Rectangle r4=naveb.getBounds();//r4 es NaveB
                            
                            if(r1.intersects(r4)&&m.EsVisible()==true && naveb.EsVisible()==true)
                            {
                                NavesBRestantes=NavesBRestantes-1;

                            m.setVisible(false);//m es misil
                            naveb.setVisible(false);
                            
                            }
                    }
                
                }
        }
    

}
