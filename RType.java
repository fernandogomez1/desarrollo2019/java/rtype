//punto nº 1 de la práctica
//punto nº 2 de la práctica
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class RType extends JFrame implements ActionListener 
{
      
    public JFrame ventana;//se crea un nuevo JFrame
    private int ResolucionX, ResolucionY; //Permite seleccionar los pixeles que tendrá la ventana del juego.
    
    
    public RType()
    {

      ResolucionX=800;//pixeles de ancho
      ResolucionY=600;//piexeles de alto
      construirVentanaPrincipal();//se llama al metodo construir ventana, en el propio metodo se detalla todo.
      /**
       * se le pasa como variable el alto y ancho del tablero, así no hay que modificar manualmente cada uno delos valores
       * en cada clase.
       */
      ventana.setSize(ResolucionX, ResolucionY);
      
    }

   
    public void actionPerformed(ActionEvent evento)//este metodo es el que está a la espera que que el jugador seleccione una opción
{
if(evento.getActionCommand().equals("Modo Facil"))//si se bulta el boton facil, se le pasan los siguientes parametros.
    {
        /**
         * se crea un nuevo controlador y se anida(en la clase controlador estan los detalles de su funcionamiento.
         */
    add(new Controlador(15, 0, ResolucionX, ResolucionY, 15));

    setSize(ResolucionX, ResolucionY);
    setLocationRelativeTo(null);
    setTitle("R-Type");//titulo de la ventana
    setFocusable(true);//para que obtenga el foco
    setResizable(false);//para no poder redimensionar la ventana
    setVisible(true); //para que muestre la ventana
    ventana.setVisible(false);//para que oculte la ventana
    ventana.remove(ventana);//para que elimine la ventana y así no crear una nueva ventana caad vez que finaliza el juego
     //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    //setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

    }
    
  /**
   * los modos normal, complicado e imposible, emplean el mismo codigo que el nivel facil,
   * lo unico que cambia son lso parametros que se le pasa al controlador para que este cree mas naves
   * y modifique la velocidad del juego.
   */
    
if(evento.getActionCommand().equals("Modo Normal"))
    {
    add(new Controlador(10, 5, ResolucionX, ResolucionY, 12));
    //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setSize(ResolucionX, ResolucionY);
    setLocationRelativeTo(null);
    setTitle("R-Type");
    setFocusable(true);
    setResizable(false);
    setVisible(true); 
    ventana.setVisible(false);
    ventana.remove(ventana);
    
    }

if(evento.getActionCommand().equals("Modo Complicado"))
    {
       
    add(new Controlador(5, 15, ResolucionX, ResolucionY, 7));
    //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setSize(ResolucionX, ResolucionY);
    setLocationRelativeTo(null);
    setTitle("R-Type");
    setFocusable(true);
    setResizable(false);
    setVisible(true);   
    ventana.setVisible(false);
    ventana.remove(ventana);
    }
    
if(evento.getActionCommand().equals("Modo Imposible"))
    {
    add(new Controlador(0, 30, ResolucionX, ResolucionY, 5));
    //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setSize(ResolucionX, ResolucionY);
    setLocationRelativeTo(null);
    setTitle("R-Type");
    setFocusable(true);
    setResizable(false);
    setVisible(true);   
    ventana.setVisible(false);
    ventana.remove(ventana);
    }
    /**
     * esto se usa para cerrar comletamente el juego.
     */
if(evento.getActionCommand().equals("Salir"))
    {
        System.exit(0);
    }
    
}


  private void construirVentanaPrincipal()
{
  ventana = new JFrame("R-Type");//crea un nuevo JFrame
  
  Container panelContenedor = ventana.getContentPane();//crea un nuevo contenedor
  ventana.setLayout(null);//selecciona el tipo de distribucion de los botones.
  
  
  JButton boton1 = new JButton("Modo Facil"); //crea un nuevo boton
  panelContenedor.add(boton1, BorderLayout.WEST);  //se elige donde colocarlo
  boton1.setBounds(40, 30, 200, 80);//setBounds(eje x, eje y, largo, ancho) se especifican las coordenadas donde se colocara el boton
  ventana.pack();//distribuye el elemento en en JFrame
  boton1.addActionListener(this);//este metodo llama al metodo actionPerformed y ejecuta lo que hay
  ventana.setVisible(true);//muestra la ventana
  
  /**
   * el resto de botones usan el mismo codigo que el boton 1, la descripcion es la misma.
   */
  
  JButton boton2 = new JButton("Modo Normal");
  panelContenedor.add(boton2, BorderLayout.EAST);
  boton2.setBounds(40, 125, 200, 80);
  ventana.pack();
  boton2.addActionListener(this);
  //ventana.setVisible(false);
  
  
  
  
  JButton boton3 = new JButton("Modo Complicado");
  panelContenedor.add(boton3, BorderLayout.NORTH);
  boton3.setBounds(40, 220, 200, 80);
  ventana.pack();
  boton3.addActionListener(this);
 // ventana.setVisible(false);
  
  
  
  JButton boton4 = new JButton("Modo Imposible");
  panelContenedor.add(boton4, BorderLayout.SOUTH);
  ventana.pack();
  boton4.setBounds(40, 315, 200, 80);
  boton4.addActionListener(this);
  //ventana.setVisible(true);
  
 JButton boton5 = new JButton("Salir");
 panelContenedor.add(boton5, BorderLayout.SOUTH);
 ventana.pack();
 boton5.setBounds(40, 410, 200, 80);
 boton5.addActionListener(this);
 //ventana.setVisible(true);
  
}
/**
 * metodo principal, construye un nuevo objeto de la clase RType
 */
  public static void main(String[] args)
    {
    new RType();  


    }
    
}