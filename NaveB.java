//Punto nº 5 de la práctica
//punto nº11b de la práctica.
//punto nº13 de la practica

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import java.awt.Rectangle;

public class NaveB extends Nave
{
 private int auxX, auxY, aux;//variable auxiliar, para recordar la posicion "x" e "y" en la que debe reaparecer la nave.
 private int MovimientoAleatorio;//variable que cada vez que se ejecute el metodo mover, va a recoger un numero aleatorio y en funcion de ese numero 
    private int Movey=0;                            // mediante unas reglas,se realiza el desplazamiento arriba, abajo o se mantiene la coordenada Y.
 
 public NaveB(int ejeX, int ejeY, String icono)//se le pasan los limites del tablero
 {
        EjeX=ejeX;
        EjeY=ejeY;
        //RutaIcono = "NaveB.jpg"; //Ruta de el icono de NaveA
        Visible=true;//poner nave visible
        ImageIcon ii = new ImageIcon(this.getClass().getResource(icono));
        INave = ii.getImage();
        width=INave.getWidth(null); //guarda en la variable width el ancho del icono.
        height = INave.getHeight(null);//guarda en la variable height el alto del icono.
        y=getAleatorio();//esto inicializa el eje "y" de la nave de manera aleatorio.
        x=getAleatorio()+600;//esto inicializa la nave en una coordenada "x" aleatoria fuera del tablero.
        auxX=x;//como el valor x va a ir variando, aux almacena el valor original de x para que reaparezca en la misma posicion.  
        mover(); //empieza a desplazar la nave
    
 }
 
 
 public void mover()//punto 14 apartado a de la guia de la practica.
{
    if (x<0)//si la nave se va  a salir por la izquierda del tablero, la recoloca por la derecha.
        {
        x=auxX;
        }
    else
    {
        x--;
        MovimientoAleatorio=getAleatorioM();
        aux=MovimientoAleatorio%600;
     
        
        if (aux>=0 && aux<=10)
    {
        Movey=1;
        
        if (y>EjeY)//añadir suma o resta para que no salga de la pantalla
            {
            Movey=2;
            }

    }
    else
        {
            
                if(aux>=590 && aux <=600)
                    {
                    Movey=2;
                        if(y<50)
                        {
                        Movey=1;
                        }
                    }
                    
            else
            {   
                if(aux>=297 && aux<=300)
                    {
                     Movey=3;
                    }
            }
           if (Movey==1){
    y++;
}
else
    {
        if (Movey==2)
        {
            y--;
        }
else
    {if (Movey==3)
        {
            y=y;
        }
    }   
}
          }
    }
}

}