//punto nº 10 de la práctica
import java.awt.Image;
import javax.swing.ImageIcon;
import java.awt.Rectangle;
public class Misil extends Nave
{
  
    private static final int VEL_MISIL=2;//velocidad del misil
    private int LimiteP;//variable para saber el limite  de la pantalla.
  


    
public Misil(int x, int y, int limitep)//añadir limitex
{  
    LimiteP=limitep;
RutaIcono="misil.png";    
ImageIcon ii = new ImageIcon(this.getClass().getResource(RutaIcono));
INave = ii.getImage();
width=INave.getWidth(null); //getWidth es un metodo de la clase image, para saber el ancho de la imagen.
    height = INave.getHeight(null);
Visible = true;
this.x=x;
this.y=y;
}

public void Mover()
{

x+=VEL_MISIL;
if(x>LimiteP)
Visible=false;
}

}